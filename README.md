# ChartPie Jetpack Compose

Este ejemplo te ayudará a crear una gráfica de pastel, chart pie o como lo conozcas.

Si ya seguiste el ejemplo y vienes por las funciones para agregar el texto. Acá te las dejo.
Van justo después de esta línea anguloActual += anguloFinal


// Calcula las coordenadas para el texto
val textX = centerX + (size.width / 3) * cos(Math.toRadians(midAngle.toDouble()).toFloat())

val textY = centerY + (size.height / 3) * sin(Math.toRadians(midAngle.toDouble()).toFloat())

// Dibuja el texto con el porcentaje

val porcentaje = (element / total * 100).toInt()

val text = "$porcentaje%"

val textPaint = Paint().asFrameworkPaint()

textPaint.color = Color.Black.toArgb()

textPaint.textSize = 55f

textPaint.isFakeBoldText = true

drawContext.canvas.nativeCanvas.drawText(text, textX, textY, textPaint)

